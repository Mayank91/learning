/* First program */
/* Array Insertion and Reading */

#include<stdio.h>
#define DIGIT
int main()
{
    float avgMarks = 0;
    float Sum = 0;
    int iteration = 0;
    int Marks[10];

    //Reading data from user
    for(iteration = 0; iteration <= 9; iteration++)
    {
        printf("\n Insert marks of students : ");
        scanf("%d",&Marks[iteration]);
    }

    int SizeOfArray = (sizeof(Marks)/sizeof(Marks[0]));

    printf("\n value %d \n",SizeOfArray);
    //Showing data inserted by user
    for(iteration = 0; iteration <= (SizeOfArray-1); iteration++)
    {
        printf("\n You have inserted : %d ", Marks[iteration]);
    }


    //calculating sum and average
    for(iteration = 0; iteration <= (SizeOfArray-1); iteration++)
    {
        Sum += Marks[iteration];
    }
    printf("\n Sum is : %f",Sum);
    avgMarks = Sum/SizeOfArray;
    printf("\n Average : %f",avgMarks);

    return 0;

}

// User input validation can be add further in this program.
// Author - Mayank Srivastava
